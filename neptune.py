from __future__ import print_function  # Python 2/3 compatibility

from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
from gremlin_python.structure.graph import Graph

from gremlin_python.process.anonymous_traversal import traversal

endpoint = "database-1.cluster-cuzmegu082ev.us-west-2.neptune.amazonaws.com"

graph = Graph()

remoteConn = DriverRemoteConnection(f'wss://{endpoint}:8182/gremlin', 'g')
g = traversal().withRemote(remoteConn)

print(g.V().limit(2).toList())
remoteConn.close()


from __future__  import print_function  # Python 2/3 compatibility

from gremlin_python import statics
from gremlin_python.structure.graph import Graph
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection

graph = Graph()
endpoint = "database-1.cluster-cuzmegu082ev.us-west-2.neptune.amazonaws.com"
reader_endpoint = "database-1.cluster-ro-cuzmegu082ev.us-west-2.neptune.amazonaws.com"

instance_endpoint = "database-1-instance-1.cuzmegu082ev.us-west-2.neptune.amazonaws.com"
print("try!")
remoteConn = DriverRemoteConnection(f'wss://{instance_endpoint}:8182/gremlin', 'g')
print("brave")
g = graph.traversal().withRemote(remoteConn)
print("delta")

print(g.V().limit(2).toList())
remoteConn.close()
print("mango")
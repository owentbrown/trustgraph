# Comment - the lambda function created by this demo
# - https://docs.aws.amazon.com/neptune/latest/userguide/get-started-cfn-lambda.html
# is f******. You have to change it as I did below if you want it to work.
# Theirs was f* because they used the prefix ws:// instead of wss://, and also because
# they used http: instead of https:


from __future__ import print_function
import boto3
import os, sys
from gremlin_python import statics
from gremlin_python.structure.graph import Graph
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
import requests

CLUSTER_ENDPOINT = os.environ['CLUSTER_ENDPOINT']
CLUSTER_PORT = os.environ['CLUSTER_PORT']


def alternate_test():
    s = "neptunedbcluster-ezt6cinet4e2.cluster-cuzmegu082ev.us-west-2.neptune.amazonaws.com"
    remoteConn = DriverRemoteConnection(f'wss://{s}:8182/gremlin', 'g')
    graph = Graph()
    g = graph.traversal().withRemote(remoteConn)

    print(g.V().limit(2).toList())
    print("alternate_test succeeded")
    remoteConn.close()


def run_sample_gremlin_websocket():
    print('running sample gremlin websocket code')
    s = "neptunedbcluster-ezt6cinet4e2.cluster-cuzmegu082ev.us-west-2.neptune.amazonaws.com"
    print(s)
    print(CLUSTER_ENDPOINT)
    # remoteConn = DriverRemoteConnection('wss://' + CLUSTER_ENDPOINT + ":" + CLUSTER_PORT + '/gremlin','g')

    print(f'wss://{s}:8182/gremlin')
    remoteConn = DriverRemoteConnection(f'wss://{s}:8182/gremlin', 'g')

    graph = Graph()
    g = graph.traversal().withRemote(remoteConn)
    print(g.V().limit(2).toList())
    print(g.V().count().next())
    remoteConn.close()


def run_sample_gremlin_http():
    print('running sample gremlin http code')
    URL = 'https://' + CLUSTER_ENDPOINT + ":" + CLUSTER_PORT + '/gremlin'
    r = requests.post(URL, data='{"gremlin":"g.V().count()"}')
    print(r.text)


def lambda_handler(event, context):
    print(event)
    print('hello from lambda handler')

    ## run gremlin query
    if CLUSTER_ENDPOINT and CLUSTER_PORT:
        alternate_test()
        run_sample_gremlin_websocket()
        run_sample_gremlin_http()

    else:
        print("provide CLUSTER_ENDPOINT and CLUSTER_PORT environment varibles")

    return "done"



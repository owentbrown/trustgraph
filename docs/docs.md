# Trustgraph docs
These docs exist because sometimes I go several days without working on this project, and it's important to remember 
where it is that I left off.

Here's the status:

I'm using these technologies:
- chalice
- jinja2
- material design light
- WTForms

The front end is ugly but basically works.
What remains is to connect this to a graph database.

I'm having trouble here. There are a lot of different points I could start from.
- Spin up an AWS Neptune instance and connect to Neptune
- Connect to local server, tinkergraph, and develop locally

There are pros and cons to both approaches. 
I have already installed TinkerGraph locally. 
I think it makes sense to use TinkerGraph to get a version working. It's free and I don't have to mess with AWS 
security groups. 
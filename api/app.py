from urllib.parse import parse_qs

from chalice import Chalice

app = Chalice(app_name='trustgraph')

import boto3
from chalice import Response
import hashlib
from jinja2 import Environment, PackageLoader, select_autoescape

import platform
import json
import os

# Load enviroment variables from config
if platform.system() == 'Darwin':
    with open(os.path.join('.chalice', 'config.json')) as f:
        d = json.load(f)
        for k, v in d["environment_variables"].items():
            os.environ[k] = v


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('trustgraph')

env = Environment(
    loader=PackageLoader('chalicelib', 'templates'),
    autoescape=select_autoescape(['html'])
)


@app.route('/')
def index():
    print(app.current_request.headers)
    if ('cookie' not in app.current_request.headers) or (app.current_request.headers['cookie'] == 'deletetme'):
        template = env.get_template('welcome.html')
        return Response(body=template.render(),
                        status_code=200,
                        headers={'Content-Type': 'text/html; charset=UTF-8'})

    else:
        template = env.get_template('index.html')
        return Response(body=template.render(),
                        status_code=200,
                        headers={'Content-Type': 'text/html; charset=UTF-8'})


@app.route('/logout', methods=['GET'])
def logout():
    return Response(body='Logging you out. You are being redirected...',
                    status_code=303,
                    headers={
                        "Set-Cookie": 'deletetme',
                        "Location": '/'
                    })


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if ('cookie' not in app.current_request.headers) or (app.current_request.headers['cookie'] == 'deleteme'):
        # TODO replace with Redirect
        return "already logged in!<br>TODO: Replace with redirect"

    if app.current_request.method == 'GET':

        template = env.get_template('signup.html')
        return Response(body=template.render(),
                        status_code=200,
                        headers={'Content-Type': 'text/html; charset=UTF-8'})

    elif app.current_request.method == 'POST':
        print("charlie")
    #     print(params)
    #
    #     return Response(body=params,
    #                     status_code=200,
    #                     headers={'Content-Type': 'text/html; charset=UTF-8'})


@app.route('/signup_post', methods=['POST'],
           content_types=['application/x-www-form-urlencoded'])
def signup_post():
    raw = app.current_request.raw_body
    s = raw.decode("utf-8")
    d = parse_qs(s)
    email = d["email"][0]
    password = d["psw"][0]

    salt = "get this from dynamo".encode()
    hashed_password = hashlib.sha512(salt + password.encode()).hexdigest()

    item = {
        "pk": f"user#{email}",
        "password": password,
        "sk": email
    }

    table.put_item(Item=item)
    html = str({"email": email, "password": hashed_password})
    return Response(body=html,
                    status_code=200,
                    headers={
                        'Content-Type': 'text/html; charset=UTF-8',
                        "Set-Cookie": email
                    })


from wtforms import Form, StringField


class AddSourceForm(Form):
    twitter_handle = StringField('Twitter Handle')


@app.route('/add_source', methods=['GET', 'POST'],
           content_types=["application/json", "application/x-www-form-urlencoded"])
def add_source():
    print("smashing")
    print(app.current_request.method)
    if app.current_request.method == 'GET':
        form = AddSourceForm()
        template = env.get_template('add_source.html')
        # "aform" because it wouldn't work if I called it form
        return Response(body=template.render(apple='beast', aform=form),
                        status_code=200,
                        headers={'Content-Type': 'text/html; charset=UTF-8'})

    if app.current_request.method == 'POST':
        return {"okay": "Time to actually get this date into a db"}


@app.route('/user/create', methods=['POST'])
def create_user():
    """Create a new user. Requires an email address and a password"""
    # If username in dynamodb, return error
    # store password in plain text for now.

    # Put only if doesn't exist
    username = app.current_request.json['username']  # Not right
    password = app.current_request.json['username']

    # check if user exists in dynamodb. If so, return error
    # if not, put item and return 200
    # Users is himself a source.


@app.route('/testneptune', methods=['GET'])
def test_neptune():
    print("started test_neptune")
    from gremlin_python.structure.graph import Graph
    from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
    graph = Graph()

    s = os.environ["NEPTUNE_CLUSTER"]
    remote_connection = DriverRemoteConnection(f'wss://{s}:8182/gremlin', 'g')
    g = graph.traversal().withRemote(remote_connection)

    s = str(g.V().limit(2).toList())

    remote_connection.close()

    return s

#
# @app.route('/source', methods=['POST', 'PUT', 'GET'])
# def source():
#     """Add a new source."""
#     if app.current_request.method == 'POST':
#         # If the source exists in DynamoDB, great.
#         # If not, add it.
#         # Add as trusted_source to user object.
#         pass
#
#     if app.current_request.method == 'PUT':
#         # Update the relationship
#         pass
#
#     if app.current_request.method == 'DELETE':
#         pass
#         # delete the current relationship
#
#
# @app.route('/test_source')
# def test_source():
#     """Takes in a URL. Gets back a score, and JSON showing 2nd and 3rd degree paths."""
#     pass


# class Source(object):
#     """A source is an entity that reports news. Sources trust other sources.
#     Each user is also associated with a single source.
#     """
#     pass
#
#
# def count_trusted_sources(truster: Source) -> int:
#     """Return the number of trusted sources"""
#     pass
#
# def add_trusted_source(truster: Source, trusted: Source):
#     """Add a new trusted source"""
#     pass
#
# def delete_trusted_source(truster: Source, trusted: Source):
#     """Delete a trusted source"""
#     pass
#
# def update_trusted_source(trusted: Source, trusted: Source):
#     """Update a trusted source"""
#     pass

# The view function above will retuarn {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}
#
# See the README documentation for more examples.
#

from __future__ import print_function  # Python 2/3 compatibility

from gremlin_python.process.anonymous_traversal import traversal
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection

g = traversal().withRemote(DriverRemoteConnection('ws://localhost:8182/gremlin', 'g'))
print("alpha")
print(type(g))
print(g.V())
print("bravo")
g.addV('person').property('name', 'marko').as_('m'). \
    addV('person').property('name', 'vadas').as_('v'). \
    addE('knows').from_('m').to('v').iterate()
print("charlie")

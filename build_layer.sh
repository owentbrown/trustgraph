# this script builds a layer.
# it doesn't use docker. Instead, you run it on an EC2 instance.
# We do it this way because they actual upload to Lambda of the layer will fail because our internet is slow.
# Lambda layers must be in this format:
#    https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers
.html#configuration-layers-path


mk dir builddir
cd builddir
sudo yum install python36
python3 -m venv venv
source venv/bin/activate
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py

vim requirements.txt
# here, you need to paste in your requirements. Remove botocore, boto3, chalice
mkdir will_rename
# note,
pip install -r requirements.txt -t will_rename
mv will_rename python

# python now refers to a directory name
zip -r full.zip python

aws lambda publish-layer-version --layer-name trust --zip-file fileb://full.zip


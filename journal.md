# Journal
## 2020-06-25 4:00 PM
In today's session, I did the following:
- I completed most of the Neptune Get Started tutorial
- I lost two hours because the Lambda implementation docs from were incorrect.
- I built a layer using AWS Lambda Layers. Unfortunately, the layer didn't work propertl.

So, what is the lesson?
- Work through the AWS tutorials. 
- Tinkerpop requires a bit of java expertise to install. I never got it to run.
- I could use a bastion server to do local Djano development with Neptune
  - https://medium.com/@ravelantunes/connecting-to-aws-neptune-from-local-environment-64c836548e89
- When I make bad tech decisions (as I did by trying to do this with Lambda instead of Django) it feels like I'm 
making progress. It feels good. Yet...my time just gets eaten up...

### Planning for next session
I think it makes sense for both the API and the front end to be managed through Django.
It will help me get a job, and it is the best thing for the project.
If I had focused on the Django route, rather than trying to get Lambda to function, then I would have made a lot more
 progress.
 
 To get Lambda to work, use the security group of the Jupyter notebook. 
 
 
 